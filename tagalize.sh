#!/usr/bin/env bash

# Tagalize v1.6.2
 echo "Tagalize v1.6.2"
#
# Author: Carlos Gonzalez Peñalba
# Email: carlos.gonzalez@ingeniamos.com
#
# You can use, modify, sell or whathever you want this piece of code,
# but this are under GNU license.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Default variables.
UNNATENDED=0
SETLATEST=0
REMOTEPUSH=0
CREATEBRANCH=0
DOTAG=1
TAGANY=0
WRITEVERSION=0
WRITEVERFILE=/dev/null
RELEASEBRANCH="false"
TYPETAG="false"
MANUALVERSION="false"
TAGDESCRIPTION="Version"

function usage {
    echo ""
    echo "Usage: $0 -t (major|minor|patch) [-m ManualVersion] [-d TagDescription] [-c ReleaseBranch] [-w FileToWriteVersion] [-u] [-l] [-r] [-p] [-a]"
    echo ""
    echo "  -t: Number to search increase. Major, Minor or Patch version."
    echo "  -m: Set manual version with format MAJOR.MINOR.PATCH (DANGER: This can broke the automatic version system)"
    echo "  -d: Set the tag description"
    echo "  -c: Limit patch increase to given release branch"
    echo "  -w: Will write the version to file"
    echo "  -u: Unnatended. It will not ask for confirmation."
    echo "  -l: Move the latest tag to the new tagged version."
    echo "  -r: Create new release branch with the calculated tag MAJOR.MINOR"
    echo "  -a: Tag always when creating a new release branch (¡Use with Caution!)"
    echo "  -p: Push changes to remote"
    echo ""
}

while getopts "d:m:t:c:w:ulrpa" opt; do
  case $opt in
    d)
      TAGDESCRIPTION=${OPTARG}
      echo "Tag description set to $OPTARG" >&2
      ;;
    m)
      MANUALVERSION=${OPTARG}
      echo "Manual version set to $OPTARG" >&2
      ;;
    t)
      TYPETAG=${OPTARG}
      echo "Will increase the $OPTARG tag" >&2
      ;;
    u)
      UNNATENDED=1
      echo "Swithing unnatended mode" >&2
      ;;
    l)
      SETLATEST=1
      echo "Will set latest tag" >&2
      ;;
    r)
      CREATEBRANCH=1
      DOTAG=0
      echo "Will only create release branch from current" >&2
      ;;
    p)
      REMOTEPUSH=1
      echo "Will push changes" >&2
      ;;
    c)
      RELEASEBRANCH=${OPTARG}
      echo "Will increase patch from release branch ${OPTARG}" >&2
      ;;
    a)
      TAGANY=1
      echo "Will tag anyways when creating a release branch" >&1
      ;;
    w)
      WRITEVERSION=1
      WRITEVERFILE=${OPTARG}
      echo "Will write the version to file: ${OPTARG}" >&1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      exit 1
      ;;
  esac
done

if [ $TAGANY -eq 1 -a $DOTAG -eq 0 ]; then
    DOTAG=1
fi

if [ "$MANUALVERSION" = "false" ]; then
    if [ "${TYPETAG}" != "major" -a "${TYPETAG}" != "minor" -a "${TYPETAG}" != "patch" ]; then
        echo "Incorrect tag type"
        usage
        exit 1
    fi

    if [ "$TYPETAG" = "patch" -a $RELEASEBRANCH != "false" ]; then
        if [[ ! "$RELEASEBRANCH" =~ ^release/[0-9]+\.[0-9]+$ ]]; then
            echo "Given branch is not a release branch"
            exit 1
        fi
        git checkout $RELEASEBRANCH 1>/dev/null 2>&1
		if [ $? -ne 0 ]; then
			echo "Cannot find the specified release branch"
			exit 1
		fi
        branchtag=$(git branch | grep "^*" | awk -F "release/" '{print $2}')
        if [[ ! "$branchtag" =~ ^[0-9]+\.[0-9]+$ ]]; then
            echo "This branch doens't have master tag"
            exit 1
        fi
        latest_tag=$(git tag -l '*.*.*' | grep "^${branchtag}" | sed "s/^V\.//g" | sed "s/^v\.//g" | sed "s/^V//g" | sed "s/^v//g" | sort -V | tail -1)
		if [ "$latest_tag" = "" ]; then
			echo "There is no tags for this release version. Setting to ${branchtag}.0"
			latest_tag=${branchtag}.0
			TYPETAG=initial
		fi
    else
        latest_tag=$(git tag -l '*.*.*' | sed "s/^V\.//g" | sed "s/^v\.//g" | sed "s/^V//g" | sed "s/^v//g" | sort -V | tail -1)
    fi

    if [[ ! "$latest_tag" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        echo "The \"latest\" tag did not exist, or did not point to a commit which also had a semantic version tag."
        exit 1
    fi

    major=$(echo "$latest_tag" | awk -F '.' '{print $1}')
    minor=$(echo "$latest_tag" | awk -F '.' '{print $2}')
    patch=$(echo "$latest_tag" | awk -F '.' '{print $3}')

    echo "Latest semantic version tag: ${major}.${minor}.${patch}"

    case "$TYPETAG" in
        initial)
            patch=0
            echo "Calculated an initial version ${major}.${minor}.${patch}"
            ;;
        major)
            echo "Bumping major version."
            major=$(( $major + 1 ))
            minor=0
            patch=0
            ;;
        minor)
            echo "Bumping minor version."
            minor=$(( $minor + 1 ))
            patch=0
            ;;
        patch|*)
            echo "Bumping patch version."
            patch=$(( $patch + 1 ))
            ;;
    esac
else
    if [[ ! "$MANUALVERSION" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        echo "The manual version passed is not valid"
        usage
        exit 1
    fi
    major=$(echo "$MANUALVERSION" | awk -F '.' '{print $1}')
    minor=$(echo "$MANUALVERSION" | awk -F '.' '{print $2}')
    patch=$(echo "$MANUALVERSION" | awk -F '.' '{print $3}')
fi

echo "New semantic version is: ${major}.${minor}.${patch}"
if [ $UNNATENDED -eq 0 ]; then
    echo -n "Do you want to tag the current commit with that version? [y/N]: "
    read answer
else
    echo "Unnateded execution. Skipping confirmation..."
    answer=y
fi

if [[ "$answer" =~ (y|Y|yes) ]]; then

    if [ $CREATEBRANCH -eq 1 ]; then
        echo "Creating new release branch release/${major}.${minor}"
        git checkout -b "release/${major}.${minor}"
    fi

	version="${major}.${minor}.${patch}"
	tag_message="${TAGDESCRIPTION} - $version"
	latest_message_version="$version"

	[ $major -eq 0 ] && tag_message="$tag_message Beta"
	[ $major -eq 0 ] && latest_message_version="$latest_message_version Beta"

    if [ $WRITEVERSION -eq 1 ]; then
        echo "Writting version $version to $WRITEVERFILE"
        echo $version >$WRITEVERFILE
    fi
	if [ $DOTAG -eq 1 ]; then
    	# Add the tag
    	git tag -a $version -m "$tag_message" > /dev/null
    	if [ $SETLATEST -eq 1 ]; then
    	    git tag -af latest -m "Latest recommended version ($latest_message_version)" > /dev/null
    	fi

    	echo "Latest tags:"
    	git tag -n1 | tail -n5
    fi
	if [ $REMOTEPUSH -eq 1 ]; then
	    echo "Pushing changes to remote"
	    if [ $CREATEBRANCH -eq 1 ]; then
	        git push origin "release/${major}.${minor}"
	    fi
	    if [ $DOTAG -eq 1 ]; then
	        git push --force --tags
	    fi
	fi

else
	echo "Aborted. No tags where added!"
fi
